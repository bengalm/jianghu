if exist Plugin*.dll (goto encrypt) else (goto decrypt)

:encrypt
if exist PluginAudio.dll (
	rename PluginAudio.dll 1.dll
	ShellCode.exe
	rename 2.dll bPluginAudio.dll
	del 1.dll
)
if exist PluginVideo.dll (
	rename PluginVideo.dll 1.dll
	ShellCode.exe
	rename 2.dll bPluginVideo.dll
	del 1.dll
)
goto exit

:decrypt
if exist bPluginAudio.dll (
	rename bPluginAudio.dll 1.dll
	ShellCode.exe
	rename 2.dll PluginAudio.dll
	del 1.dll
)
if exist bPluginVideo.dll (
	rename bPluginVideo.dll 1.dll
	ShellCode.exe
	rename 2.dll PluginVideo.dll
	del 1.dll
)
goto exit

:exit
