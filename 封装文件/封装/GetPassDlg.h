#if !defined(AFX_GETPASSDLG_H__8141C673_489D_4A89_818F_2D1BEFA0E531__INCLUDED_)
#define AFX_GETPASSDLG_H__8141C673_489D_4A89_818F_2D1BEFA0E531__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GetPassDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GetPassDlg dialog

class GetPassDlg : public CDialog
{
// Construction
public:
	CString m_Pass;
	GetPassDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(GetPassDlg)
	enum { IDD = IDD_DIALOG1 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GetPassDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GetPassDlg)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GETPASSDLG_H__8141C673_489D_4A89_818F_2D1BEFA0E531__INCLUDED_)
