// 文件保险箱Dlg.h : header file
//

#if !defined(AFX_DLG_H__19006A79_9FEC_433F_B860_FF81C23292FE__INCLUDED_)
#define AFX_DLG_H__19006A79_9FEC_433F_B860_FF81C23292FE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CMyDlg dialog
typedef struct _alglist//当前USBKEY证书信息
{
	
	char  strAlgName[128];	//算法名称
	int		nAlgID;			//算法ID
}ALG_LIST,*ALG_LIST_PTR;

class CMyDlg : public CDialog
{
// Construction
public:
	BOOL Decrypt_File(CString strCipherFilePath, CString strPlainFilePath,CString strPass);
	BOOL Encrypt_File(CString strPlainFilePath,CString strCipherFilePath,DWORD nAlg_ID,CString strPass);
	CMyDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMyDlg)
	enum { IDD = IDD_MY_DIALOG };
	CComboBox	m_ALGLIST;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMyDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton4();
	afx_msg void OnButton5();
	afx_msg void OnButton3();
	afx_msg void OnButton6();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_H__19006A79_9FEC_433F_B860_FF81C23292FE__INCLUDED_)
