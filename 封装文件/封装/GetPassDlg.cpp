// GetPassDlg.cpp : implementation file
//

#include "stdafx.h"
#include "�ļ�������.h"
#include "GetPassDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GetPassDlg dialog


GetPassDlg::GetPassDlg(CWnd* pParent /*=NULL*/)
	: CDialog(GetPassDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GetPassDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void GetPassDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GetPassDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GetPassDlg, CDialog)
	//{{AFX_MSG_MAP(GetPassDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GetPassDlg message handlers

void GetPassDlg::OnOK() 
{
	GetDlgItemText(IDC_EDIT_PASS,m_Pass);	
	CDialog::OnOK();
}

void GetPassDlg::OnCancel() 
{
 	m_Pass.Empty();
	CDialog::OnCancel();
}
