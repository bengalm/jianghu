//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Player.rc
//
#define IDD_PLAYER_DIALOG               102
#define IDR_MAINFRAME                   128
#define IDB_HORIZONTAL_SCROLLBAR_CHANNEL 159
#define IDB_HORIZONTAL_SCROLLBAR_LEFTARROW 160
#define IDB_HORIZONTAL_SCROLLBAR_RIGHTARROW 161
#define IDB_HORIZONTAL_SCROLLBAR_THUMB  162
#define IDB_HORIZONTAL_SCROLLBAR_THUMB_NO_COLOR 163
#define IDB_VERTICAL_SCROLLBAR_CHANNEL  164
#define IDB_VERTICAL_SCROLLBAR_DOWNARROW 165
#define IDB_VERTICAL_SCROLLBAR_THUMB    166
#define IDB_VERTICAL_SCROLLBAR_THUMB_NO_COLOR 167
#define IDB_VERTICAL_SCROLLBAR_UPARROW  168
#define IDC_VIDEO_WINDOW                1000
#define IDC_BTN_OPEN                    1001
#define IDC_BTN_PLAY                    1003
#define IDC_BTN_PAUSE                   1004
#define IDC_BTN_STOP                    1005
#define IDC_SCROLL_RECT_HORZ            1007
#define IDC_SCROLL_HORZ                 1008
#define IDC_STATIC_TIS                  1008
#define IDC_SCROLL_RECT_VOICE           1009
#define IDC_SCROLL_VOICE                1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        174
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
