; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CPlayerDlg
LastTemplate=CSliderCtrl
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Player.h"

ClassCount=2
Class1=CPlayerApp
Class2=CPlayerDlg

ResourceCount=2
Resource1=IDR_MAINFRAME
Resource2=IDD_PLAYER_DIALOG

[CLS:CPlayerApp]
Type=0
HeaderFile=Player.h
ImplementationFile=Player.cpp
Filter=N
BaseClass=CWinApp
VirtualFilter=AC
LastObject=CPlayerApp

[CLS:CPlayerDlg]
Type=0
HeaderFile=PlayerDlg.h
ImplementationFile=PlayerDlg.cpp
Filter=W
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CPlayerDlg



[DLG:IDD_PLAYER_DIALOG]
Type=1
Class=CPlayerDlg
ControlCount=7
Control1=IDC_BTN_OPEN,button,1342242816
Control2=IDC_BTN_PLAY,button,1342242816
Control3=IDC_BTN_PAUSE,button,1342242816
Control4=IDC_BTN_STOP,button,1342242816
Control5=IDC_VIDEO_WINDOW,static,1342177284
Control6=IDC_SCROLL_RECT_HORZ,static,1350696960
Control7=IDC_STATIC_TIS,static,1342308352

