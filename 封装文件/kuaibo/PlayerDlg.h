// PlayerDlg.h : header file
//
//{{AFX_INCLUDES()
#include "shockwaveflash.h"
//}}AFX_INCLUDES

#if !defined(AFX_PLAYERDLG_H__478DDD21_82CD_484B_A4B4_F0823E52F2D7__INCLUDED_)
#define AFX_PLAYERDLG_H__478DDD21_82CD_484B_A4B4_F0823E52F2D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CPlayerDlg dialog

#include "XScrollBar.h"
#include "shockwaveflash.h"

#define SCREEN_WIDTH	320
#define SCREEN_HEIGHT	240

#define MINX			320
#define MINY			240

#define SLIDER_TIMER	100
#define Count_TIMER		101

class CPlayerDlg : public CDialog
{
// Construction
public:
	CPlayerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CPlayerDlg)
	enum { IDD = IDD_PLAYER_DIALOG };
	CStatic	m_Tis;
	CXScrollBar	m_FrameScrollBar;
	CStatic	mVideoWindow;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPlayerDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	CShockwaveFlash*	m_pFlash;
	CString				mSourceFile;	// 源文件
	UINT				mSliderTimer;	// 定时器ID
	UINT				mCountTimer;	// 定时器ID

	void CreateFlash(void);        // 创建Flash
	void DestroyFlash(void);       // 析构Flash

	void ResetSize();

	CString GetTimeStr(int nTime);

	// Generated message map functions
	//{{AFX_MSG(CPlayerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnBtnOpen();
	afx_msg void OnBtnPlay();
	afx_msg void OnBtnPause();
	afx_msg void OnBtnStop();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PLAYERDLG_H__478DDD21_82CD_484B_A4B4_F0823E52F2D7__INCLUDED_)
