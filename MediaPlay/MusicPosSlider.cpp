// MusicPosSlider.cpp : implementation file
//

#include "stdafx.h"
#include "MediaPlay2.h"
#include "MusicPosSlider.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMusicPosSlider

CMusicPosSlider::CMusicPosSlider():m_point(12,113)
{
	m_Flag=FALSE;
}

CMusicPosSlider::~CMusicPosSlider()
{
	m_Flag=FALSE;
}


BEGIN_MESSAGE_MAP(CMusicPosSlider, CButton)
	//{{AFX_MSG_MAP(CMusicPosSlider)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_PAINT()
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMusicPosSlider message handlers

void CMusicPosSlider::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	m_Flag=TRUE;
//	CButton::OnLButtonDown(nFlags, point);
}

void CMusicPosSlider::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	ClientToScreen(&point);
	(this->GetParent())->ScreenToClient(&point);
	if (m_Flag)
	{
	    SetMusicPos(point.x-12);
	}
	CButton::OnMouseMove(nFlags, point);
}

void CMusicPosSlider::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	CBitmap bitmap;
	bitmap.LoadBitmap(IDB_BITMAP_POS);
	CRect rect;
	GetClientRect(&rect);
	CDC dcCompatible;
	dcCompatible.CreateCompatibleDC(&dc);
	dcCompatible.SelectObject(&bitmap);
	BITMAP bmp;
	bitmap.GetBitmap(&bmp);
	if(dc.StretchBlt(0,0,rect.Width(),rect.Height(),&dcCompatible,0,0,bmp.bmWidth,bmp.bmHeight,SRCCOPY)==FALSE)
	{
		MessageBox("FALSE");
	}
	// Do not call CButton::OnPaint() for painting messages
}

void CMusicPosSlider::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	m_Flag=FALSE;
	CButton::OnLButtonUp(nFlags, point);
}


void CMusicPosSlider::SetMusicPos(int Size)
{
  if ((m_point.x+Size)<115)
   {
		SetWindowPos(NULL,m_point.x+Size,115,0,0,SWP_NOSIZE |SWP_NOZORDER);
   }
  else
  {
	  SetWindowPos(NULL,115,115,0,0,SWP_NOSIZE |SWP_NOZORDER);
  }
}
