// MediaPlay2Dlg.h : header file
//

#if !defined(AFX_MEDIAPLAY2DLG_H__179DFDB8_8291_4EFC_A4A0_58668F20E1CB__INCLUDED_)
#define AFX_MEDIAPLAY2DLG_H__179DFDB8_8291_4EFC_A4A0_58668F20E1CB__INCLUDED_

#include "MusicListView.h"
#include "MusicPosSlider.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CMediaPlay2Dlg dialog

class CMediaPlay2Dlg : public CDialog
{
// Construction
public:
	int m_nWidth;
	DWORD m_MusicLengh;
	CString  m_Music_name;
	UINT m_Device;
	CMediaPlay2Dlg(CWnd* pParent = NULL);	// standard constructor
    CMusicListView m_MusicList;
// Dialog Data
	//{{AFX_DATA(CMediaPlay2Dlg)
	enum { IDD = IDD_MEDIAPLAY2_DIALOG };
	CMusicPosSlider	m_btn_pos;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMediaPlay2Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMediaPlay2Dlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBtnClose();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBtnXiao();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMusicSlider();
	afx_msg void OnBtnListview();
	afx_msg void OnBtnPlay();
	afx_msg void OnBtnNext();
	afx_msg void OnBtnLast();
	afx_msg void OnBtnStop();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
//	CBitmapButton m_btn_pos;
	CBitmapButton m_btn_voice_move;
	CBitmapButton m_btn_voice;
	CBitmapButton m_btn_list;
	CBitmapButton m_btn_last;
	CBitmapButton m_btn_next;
	CBitmapButton m_btnmap;
	CBitmapButton m_btn_stop;
	CBitmapButton m_btn_play;
	CBitmapButton m_bttmap_close;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MEDIAPLAY2DLG_H__179DFDB8_8291_4EFC_A4A0_58668F20E1CB__INCLUDED_)
