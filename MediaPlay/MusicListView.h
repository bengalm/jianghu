#if !defined(AFX_MUSICLISTVIEW_H__8407D2DA_7C7E_434D_9627_9A1CEFF55031__INCLUDED_)
#define AFX_MUSICLISTVIEW_H__8407D2DA_7C7E_434D_9627_9A1CEFF55031__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MusicListView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMusicListView dialog

class CMusicListView : public CDialog
{
// Construction
public:
	HANDLE m_Playlisthandle;
	HWND m_Parent_hwnd;
	BOOL m_ShowFlag;  //��--���� ��----����
	CMusicListView(CWnd* pParent = NULL);   // standard constructor
    ~CMusicListView();
// Dialog Data
	//{{AFX_DATA(CMusicListView)
	enum { IDD = IDD_DIALOG_LIST };
	CListCtrl	m_Music_list;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMusicListView)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMusicListView)
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	virtual BOOL OnInitDialog();
	afx_msg void OnBtnAddFloud();
	afx_msg void OnBtnDelete();
	afx_msg void OnBtnAddMp3();
	afx_msg void OnDblclkListPlay(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CBitmapButton m_btn_delete;
	CBitmapButton m_btn_add_mp3;
	CBitmapButton m_btn_add_floud;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MUSICLISTVIEW_H__8407D2DA_7C7E_434D_9627_9A1CEFF55031__INCLUDED_)
