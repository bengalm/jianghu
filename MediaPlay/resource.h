//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by MediaPlay2.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MEDIAPLAY2_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP_MAIN                 129
#define IDB_BITMAP_XIAO                 130
#define IDB_BITMAP_CLOSE                131
#define IDB_BITMAP_PLAY                 132
#define IDB_BITMAP_LAST                 133
#define IDB_BITMAP_NEXT                 134
#define IDB_BITMAP_STOP                 135
#define IDB_BITMAP_STOP_CLICK           136
#define IDB_BITMAP_LAST_CLICK           137
#define IDB_BITMAP_CLOSE_CLICK          138
#define IDB_BITMAP_NEXT_CLICK           139
#define IDB_BITMAP_PLAY_CLICK           140
#define IDB_BITMAP_XIAO_CLICK           141
#define IDB_BITMAP_LISTVIEW_CLICK       142
#define IDB_BITMAP_LISTVIEW             143
#define IDB_BITMAP_POS_DI               144
#define IDB_BITMAP_POS                  145
#define IDD_DIALOG_LIST                 146
#define IDB_BITMAP_MUSICLIST            147
#define IDB_BITMAP_POS_CLICK            149
#define IDB_BITMAP_VOICE                151
#define IDB_BITMAP_DELETE               152
#define IDB_BITMAP_DELETE_CLICK         153
#define IDB_BITMAP_ADD_FOD              154
#define IDB_BITMAP_ADD_FOD_CLICK        155
#define IDB_BITMAP_ADD_MP3              156
#define IDB_BITMAP_ADD_MP3_CLICK        157
#define IDB_BITMAP_VOICE_MOVE           158
#define IDC_BTN_XIAO                    1000
#define IDC_BTN_CLOSE                   1001
#define IDC_BTN_PLAY                    1002
#define IDC_BTN_STOP                    1003
#define IDC_BTN_NEXT                    1004
#define IDC_BTN_LAST                    1005
#define IDC_BTN_LISTVIEW                1007
#define IDC_BTN_POS_DI                  1017
#define IDC_SLIDER1                     1020
#define IDC_MUSIC_SLIDER                1022
#define IDC_BTN_POS                     1025
#define IDC_BTN_VOICE                   1026
#define IDC_LIST_PLAY                   1027
#define IDC_BTN_DELETE                  1028
#define IDC_BTN_ADD_MP3                 1029
#define IDC_BTN_ADD_FLOUD               1030
#define IDC_LIST1                       1031
#define IDC_BTN_VOICE_MOVE              1032

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        159
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1033
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
