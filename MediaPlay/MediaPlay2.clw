; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CMusicListView
LastTemplate=CButton
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "MediaPlay2.h"

ClassCount=6
Class1=CMediaPlay2App
Class2=CMediaPlay2Dlg
Class3=CAboutDlg

ResourceCount=4
Resource1=IDD_MEDIAPLAY2_DIALOG
Resource2=IDR_MAINFRAME
Class4=CMusicPos
Resource3=IDD_ABOUTBOX
Class5=CMusicListView
Class6=CMusicPosSlider
Resource4=IDD_DIALOG_LIST

[CLS:CMediaPlay2App]
Type=0
HeaderFile=MediaPlay2.h
ImplementationFile=MediaPlay2.cpp
Filter=N

[CLS:CMediaPlay2Dlg]
Type=0
HeaderFile=MediaPlay2Dlg.h
ImplementationFile=MediaPlay2Dlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_BTN_POS

[CLS:CAboutDlg]
Type=0
HeaderFile=MediaPlay2Dlg.h
ImplementationFile=MediaPlay2Dlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_MEDIAPLAY2_DIALOG]
Type=1
Class=CMediaPlay2Dlg
ControlCount=11
Control1=IDC_BTN_XIAO,button,1342242827
Control2=IDC_BTN_CLOSE,button,1342242827
Control3=IDC_BTN_PLAY,button,1342242827
Control4=IDC_BTN_STOP,button,1342242827
Control5=IDC_BTN_NEXT,button,1342242827
Control6=IDC_BTN_LAST,button,1342242827
Control7=IDC_BTN_LISTVIEW,button,1342242827
Control8=IDC_MUSIC_SLIDER,static,1342177541
Control9=IDC_BTN_POS,button,1342242827
Control10=IDC_BTN_VOICE,button,1342242827
Control11=IDC_BTN_VOICE_MOVE,button,1342242827

[CLS:CMusicPos]
Type=0
HeaderFile=MusicPos.h
ImplementationFile=MusicPos.cpp
BaseClass=CStatic
Filter=W
VirtualFilter=WC
LastObject=CMusicPos

[DLG:IDD_DIALOG_LIST]
Type=1
Class=CMusicListView
ControlCount=4
Control1=IDC_LIST_PLAY,SysListView32,1350631427
Control2=IDC_BTN_DELETE,button,1342242827
Control3=IDC_BTN_ADD_MP3,button,1342242827
Control4=IDC_BTN_ADD_FLOUD,button,1342242827

[CLS:CMusicListView]
Type=0
HeaderFile=MusicListView.h
ImplementationFile=MusicListView.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_LIST_PLAY
VirtualFilter=dWC

[CLS:CMusicPosSlider]
Type=0
HeaderFile=MusicPosSlider.h
ImplementationFile=MusicPosSlider.cpp
BaseClass=CButton
Filter=W
VirtualFilter=BWC

