// MediaPlay2Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "MediaPlay2.h"
#include "MediaPlay2Dlg.h"
#include <MMSYSTEM.H>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma comment(lib,"Winmm.lib")
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMediaPlay2Dlg dialog

CMediaPlay2Dlg::CMediaPlay2Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMediaPlay2Dlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMediaPlay2Dlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_Device=0;
	m_nWidth=18;
}

void CMediaPlay2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMediaPlay2Dlg)
	DDX_Control(pDX, IDC_BTN_POS, m_btn_pos);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMediaPlay2Dlg, CDialog)
	//{{AFX_MSG_MAP(CMediaPlay2Dlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_CLOSE, OnBtnClose)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BTN_XIAO, OnBtnXiao)
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_MUSIC_SLIDER, OnMusicSlider)
	ON_BN_CLICKED(IDC_BTN_LISTVIEW, OnBtnListview)
	ON_BN_CLICKED(IDC_BTN_PLAY, OnBtnPlay)
	ON_BN_CLICKED(IDC_BTN_NEXT, OnBtnNext)
	ON_BN_CLICKED(IDC_BTN_LAST, OnBtnLast)
	ON_BN_CLICKED(IDC_BTN_STOP, OnBtnStop)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMediaPlay2Dlg message handlers


BOOL CMediaPlay2Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	///////////////////初始化按钮//////////////
	m_btnmap.LoadBitmaps(IDB_BITMAP_XIAO,IDB_BITMAP_XIAO_CLICK);
	m_btnmap.SubclassDlgItem(IDC_BTN_XIAO,this);
	m_btnmap.SizeToContent();
	m_bttmap_close.LoadBitmaps(IDB_BITMAP_CLOSE,IDB_BITMAP_CLOSE_CLICK);
	m_bttmap_close.SubclassDlgItem(IDC_BTN_CLOSE,this);
	m_bttmap_close.SizeToContent();
	m_btn_play.LoadBitmaps(IDB_BITMAP_PLAY,IDB_BITMAP_PLAY_CLICK);
	m_btn_play.SubclassDlgItem(IDC_BTN_PLAY,this);
	m_btn_play.SizeToContent();
	m_btn_stop.LoadBitmaps(IDB_BITMAP_STOP,IDB_BITMAP_STOP_CLICK);
	m_btn_stop.SubclassDlgItem(IDC_BTN_STOP,this);
	m_btn_stop.SizeToContent();
	m_btn_next.LoadBitmaps(IDB_BITMAP_NEXT,IDB_BITMAP_NEXT_CLICK);
	m_btn_next.SubclassDlgItem(IDC_BTN_NEXT,this);
	m_btn_next.SizeToContent();
	m_btn_last.LoadBitmaps(IDB_BITMAP_LAST,IDB_BITMAP_LAST_CLICK);
	m_btn_last.SubclassDlgItem(IDC_BTN_LAST,this);
	m_btn_last.SizeToContent();
	m_btn_list.LoadBitmaps(IDB_BITMAP_LISTVIEW,IDB_BITMAP_LISTVIEW_CLICK);
	m_btn_list.SubclassDlgItem(IDC_BTN_LISTVIEW,this);
	m_btn_list.SizeToContent();
	m_btn_pos.LoadBitmaps(IDB_BITMAP_POS,IDB_BITMAP_POS_CLICK);
	m_btn_pos.SizeToContent();
	m_btn_voice.LoadBitmaps(IDB_BITMAP_VOICE);
	m_btn_voice.SubclassDlgItem(IDC_BTN_VOICE,this);
	m_btn_voice.SizeToContent();
	m_btn_voice_move.LoadBitmaps(IDB_BITMAP_VOICE_MOVE);
	m_btn_voice_move.SubclassDlgItem(IDC_BTN_VOICE_MOVE,this);
	m_btn_voice_move.SizeToContent();
	////////////////////////////////////////////////
    //  
    m_MusicList.Create(IDD_DIALOG_LIST,NULL);
	m_MusicList.ShowWindow(SW_SHOW);
	m_MusicList.m_ShowFlag=TRUE;
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMediaPlay2Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMediaPlay2Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
//		CDialog::OnPaint();
		CBitmap bitmap;
		bitmap.LoadBitmap(IDB_BITMAP_MAIN);
		CPaintDC dc(this);
		CRect rect;
		GetClientRect(&rect);
		CDC dcCompatible;
		dcCompatible.CreateCompatibleDC(&dc);
		dcCompatible.SelectObject(&bitmap);
		BITMAP bmp;
		bitmap.GetBitmap(&bmp);
		if(dc.StretchBlt(0,0,rect.Width(),rect.Height(),&dcCompatible,0,0,bmp.bmWidth,bmp.bmHeight,SRCCOPY)==FALSE)
		{
			MessageBox("FALSE");
		}
/*		dc.SetBkMode(TRANSPARENT);
	//	dc.SetBkColor(RGB(255,255,255));
		dc.SetTextColor(RGB(3,252,40));
	    dc.TextOut(18,55,m_Music_name);*/
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMediaPlay2Dlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CMediaPlay2Dlg::OnBtnClose() 
{
	// TODO: Add your control notification handler code here
	if (MessageBox("你确定要退出","提示",MB_OKCANCEL)==IDOK)
	{
			EndDialog(0);
	}
}



void CMediaPlay2Dlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CDialog::OnLButtonDown(nFlags, point);
	PostMessage(WM_NCLBUTTONDOWN,HTCAPTION,0);
}

void CMediaPlay2Dlg::OnBtnXiao() 
{
	// TODO: Add your control notification handler code here
	ShowWindow(SW_MINIMIZE);
}

void CMediaPlay2Dlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CDialog::OnLButtonUp(nFlags, point);
}

void CMediaPlay2Dlg::OnMusicSlider() 
{
	// TODO: Add your control notification handler code here
	POINT point;
	GetCursorPos(&point);
	ScreenToClient(&point);
    m_btn_pos.SetMusicPos(point.x-12);
	if (!m_Device)
	{
		MCI_SEEK_PARMS seekParam;
		MCIERROR mciError;
		seekParam.dwTo=((double)(point.x-12)/100)*m_MusicLengh;
		mciError=mciSendCommand(m_Device,MCI_SEEK,MCI_TO |MCI_WAIT,(DWORD)(LPVOID)&seekParam);
		if (mciError)
		{
			CString str;
			mciGetErrorString(mciError,str.LockBuffer(),128);
			//		MessageBox(str);
			return;
		}
	}
//	SendMessage(m_Device,(WPARAM)On,0);
}

// Downloads By http://www.veryhuo.com

void CMediaPlay2Dlg::OnBtnListview() 
{
	// TODO: Add your control notification handler code here
	if (m_MusicList.m_ShowFlag)
	{
		m_MusicList.ShowWindow(SW_HIDE);
		m_MusicList.m_ShowFlag=FALSE;
	}
    else
	{
      m_MusicList.ShowWindow(SW_SHOW);
	  m_MusicList.m_ShowFlag=TRUE;
	}

}
CString memfind(const char *mem,const char * str2)   
{   
	int   da2,i,j,m,sizem;
	char temp[40]={0};
	sizem=strlen(mem);
	da2 = strlen(str2);
	for (m = 0;m < sizem; m++)   
	{   
		for (j = 0; j < da2; j ++)   
			if (mem[m+j] != str2[j])	
				break;   
		if (j == da2) 		
				break;   
	}
	for (i = m-3; i > 0; i--)   
	{   
		if (mem[i]=='\\')
		{
			i++;
            for (int n=i;n<m;n++)
            {
				temp[n-i]=mem[n];
            }
			break;
		}
	}
	CString strtemp(temp);
    return strtemp;
}

void CMediaPlay2Dlg::OnBtnPlay() 
{
	// TODO: Add your control notification handler code here
	if (m_Device==0)
	{
		m_nWidth=18;
		 Invalidate(TRUE);
        char MusicPath[MAX_PATH]={0};
		if (m_MusicList.m_Music_list.GetSelectionMark()==-1)
		{
			CFileDialog cfd(TRUE,NULL,NULL,OFN_HIDEREADONLY |OFN_OVERWRITEPROMPT,"mp3文件(*.mp3)|*.mp3|所有文件(*.*)|*.*|",NULL);
			if (cfd.DoModal()==IDOK)
			{
				DWORD dwWrite;
				strcpy(MusicPath,cfd.m_ofn.lpstrFile);
				strcat(MusicPath,"\r\n");
				WriteFile(m_MusicList.m_Playlisthandle,MusicPath,MAX_PATH,&dwWrite,NULL);
				m_MusicList.m_Music_list.InsertItem(0,MusicPath,NULL);
			}
		}
		else
		{
			m_MusicList.m_Music_list.GetItemText(m_MusicList.m_Music_list.GetSelectionMark(),0,MusicPath,MAX_PATH);
		}
		CString temp(MusicPath);
		strcpy(MusicPath,temp.SpanExcluding("\r\n").LockBuffer());
		m_Music_name=memfind(MusicPath,".mp3");
		MCI_OPEN_PARMS mciOpen;
		MCIERROR mciError;
		mciOpen.lpstrElementName=MusicPath;
		if (m_Device)
		{
			mciError=mciSendCommand(m_Device,MCI_CLOSE,NULL,NULL);
			if (mciError)
			{
				CString str;
				mciGetErrorString(mciError,str.LockBuffer(),128);
				MessageBox(str);
				return;
			}
		}
		mciError=mciSendCommand(0,MCI_OPEN,MCI_OPEN_ELEMENT,(DWORD)&mciOpen);
		if (mciError)
		{
			CString str;
			mciGetErrorString(mciError,str.LockBuffer(),128);
		//	MessageBox(str);
			return;
		}
		m_Device=mciOpen.wDeviceID;
		MCI_PLAY_PARMS mciPlay;
		mciError=mciSendCommand(m_Device,MCI_PLAY,NULL,(DWORD)&mciPlay);
		if (mciError)
		{
			CString str;
			mciGetErrorString(mciError,str.LockBuffer(),128);
	//		MessageBox(str);
			return;
		}
//	    Invalidate(TRUE);
		MCI_STATUS_PARMS mciStatus;
		mciStatus.dwItem=MCI_STATUS_LENGTH;
		mciError=mciSendCommand(m_Device, MCI_STATUS,MCI_STATUS_ITEM,(DWORD)&mciStatus);
		if (mciError)
		{
			CString str;
			mciGetErrorString(mciError,str.LockBuffer(),128);
			MessageBox(str);
			return;
		}
		m_MusicLengh=mciStatus.dwReturn;
		SetTimer(1,2000,NULL);
	}
	else
	{
		MCI_STATUS_PARMS mciStatus;
		MCIERROR mciError;
		mciStatus.dwItem=MCI_STATUS_MODE;
		mciError=mciSendCommand(m_Device, MCI_STATUS,MCI_STATUS_ITEM,(DWORD)&mciStatus);
		if (mciError)
		{
			CString str;
			mciGetErrorString(mciError,str.LockBuffer(),128);
			MessageBox(str);
			return;
		}
		switch (mciStatus.dwReturn)
		{
			case MCI_MODE_PLAY:
				{
					mciError=mciSendCommand(m_Device,MCI_PAUSE,NULL,NULL);
					if (mciError)
					{
						CString str;
						mciGetErrorString(mciError,str.LockBuffer(),128);
						MessageBox(str);
						return;
					}
					break;
				}
			case MCI_MODE_PAUSE:
				{
					MCI_OPEN_PARMS mciPlay;
					mciError=mciSendCommand(m_Device,MCI_PLAY,NULL,(DWORD)&mciPlay);
					if (mciError)
					{
						CString str;
						mciGetErrorString(mciError,str.LockBuffer(),128);
						MessageBox(str);
						return;
					}
					break;
				}
			default:
				{
					m_nWidth=18;
					 Invalidate(TRUE);
					char MusicPath[MAX_PATH]={0};
					if (m_MusicList.m_Music_list.GetSelectionMark()==-1)
					{
						CFileDialog cfd(TRUE,NULL,NULL,OFN_HIDEREADONLY |OFN_OVERWRITEPROMPT,"mp3文件(*.mp3)|*.mp3|所有文件(*.*)|*.*|",NULL);
						if (cfd.DoModal()==IDOK)
						{
							DWORD dwWrite;
							strcpy(MusicPath,cfd.m_ofn.lpstrFile);
							strcat(MusicPath,"\r\n");
							WriteFile(m_MusicList.m_Playlisthandle,MusicPath,MAX_PATH,&dwWrite,NULL);
							m_MusicList.m_Music_list.InsertItem(0,MusicPath,NULL);
						}
					}
					else
					{
						m_MusicList.m_Music_list.GetItemText(m_MusicList.m_Music_list.GetSelectionMark(),0,MusicPath,MAX_PATH);
					}
					CString temp(MusicPath);
					strcpy(MusicPath,temp.SpanExcluding("\r\n").LockBuffer());
					m_Music_name=memfind(MusicPath,".mp3");
					MCI_OPEN_PARMS mciOpen;
					MCIERROR mciError;
					mciOpen.lpstrElementName=MusicPath;
					if (m_Device)
					{
						mciError=mciSendCommand(m_Device,MCI_CLOSE,NULL,NULL);
						if (mciError)
						{
							CString str;
							mciGetErrorString(mciError,str.LockBuffer(),128);
							MessageBox(str);
							return;
						}
					}
					mciError=mciSendCommand(0,MCI_OPEN,MCI_OPEN_ELEMENT,(DWORD)&mciOpen);
					if (mciError)
					{
						CString str;
						mciGetErrorString(mciError,str.LockBuffer(),128);
						MessageBox(str);
						return;
					}
					m_Device=mciOpen.wDeviceID;
					MCI_PLAY_PARMS mciPlay;
					mciError=mciSendCommand(m_Device,MCI_PLAY,NULL,(DWORD)&mciPlay);
					if (mciError)
					{
						CString str;
						mciGetErrorString(mciError,str.LockBuffer(),128);
						MessageBox(str);
						return;
					}
//	                Invalidate(TRUE);	
					MCI_STATUS_PARMS mciStatus;
					mciStatus.dwItem=MCI_STATUS_LENGTH;
					mciError=mciSendCommand(m_Device, MCI_STATUS,MCI_STATUS_ITEM,(DWORD)&mciStatus);
					if (mciError)
					{
						CString str;
						mciGetErrorString(mciError,str.LockBuffer(),128);
						MessageBox(str);
						return;
					}
		            m_MusicLengh=mciStatus.dwReturn;
					break;
				}
		}
	}

}



void CMediaPlay2Dlg::OnBtnNext() 
{
	// TODO: Add your control notification handler code here
   int Select;
   MCI_GENERIC_PARMS mciStop;
   MCIERROR mciError;
   mciError=mciSendCommand(m_Device, MCI_STOP,NULL,(DWORD)&mciStop);
   if (mciError)
   {
	   CString str;
	   mciGetErrorString(mciError,str.LockBuffer(),128);
	   MessageBox(str);
	   return;
   }
   Select=m_MusicList.m_Music_list.GetNextItem(m_MusicList.m_Music_list.GetSelectionMark(),LVNI_BELOW);
   if (Select==-1)
   {
	   	m_MusicList.m_Music_list.SetSelectionMark(0);
   }
   else
   {
        m_MusicList.m_Music_list.SetSelectionMark(Select);
   }

   SendMessage(WM_COMMAND,(WPARAM)IDC_BTN_PLAY,0);
}

void CMediaPlay2Dlg::OnBtnLast() 
{
	// TODO: Add your control notification handler code here
   MCI_GENERIC_PARMS mciStop;
   MCIERROR mciError;
   mciError=mciSendCommand(m_Device, MCI_STOP,NULL,(DWORD)&mciStop);
   if (mciError)
   {
	   CString str;
	   mciGetErrorString(mciError,str.LockBuffer(),128);
	   MessageBox(str);
	   return;
   }
   int Select;
   Select=m_MusicList.m_Music_list.GetNextItem(m_MusicList.m_Music_list.GetSelectionMark(),LVNI_ABOVE);
   if (Select==-1)
   {
	   m_MusicList.m_Music_list.SetSelectionMark(m_MusicList.m_Music_list.GetItemCount()-1);
   }
   else
   {
	   m_MusicList.m_Music_list.SetSelectionMark(Select);
   }
	SendMessage(WM_COMMAND,(WPARAM)IDC_BTN_PLAY,0);
}

void CMediaPlay2Dlg::OnBtnStop() 
{
	// TODO: Add your control notification handler code here
   MCI_GENERIC_PARMS mciStop;
   MCIERROR mciError;
   mciError=mciSendCommand(m_Device, MCI_STOP,NULL,(DWORD)&mciStop);
   if (mciError)
   {
	   CString str;
	   mciGetErrorString(mciError,str.LockBuffer(),128);
//	   MessageBox(str);
	   return;
   }
   m_btn_pos.SetMusicPos(0);

}

void CMediaPlay2Dlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	MCI_STATUS_PARMS mciStatus;
	MCIERROR mciError;
	mciStatus.dwItem=MCI_STATUS_POSITION;
	mciError=mciSendCommand(m_Device, MCI_STATUS,MCI_STATUS_ITEM,(DWORD)&mciStatus);
	if (mciError)
	{
		CString str;
		mciGetErrorString(mciError,str.LockBuffer(),128);
		MessageBox(str);
		return;
	}
	double pos=((double)mciStatus.dwReturn/m_MusicLengh)*100;
    m_btn_pos.SetMusicPos(pos);
	if (pos==100)
	{
        SendMessage(WM_COMMAND,(WPARAM)IDC_BTN_NEXT,0);
	}
	CClientDC dc(this);
	TEXTMETRIC tm;
	dc.GetTextMetrics(&tm);
	CRect rect;
	m_nWidth+=10;
	if (m_nWidth>=226)
	{
		m_nWidth=18;
		Invalidate(TRUE);
	}
    rect.left=18;
	rect.top=55;
	rect.right=m_nWidth;
	rect.bottom=rect.top+tm.tmHeight;

	dc.SetBkMode(TRANSPARENT);
    dc.SetTextColor(RGB(3,252,40));
	dc.DrawText(m_Music_name,rect,DT_LEFT);
    

	CDialog::OnTimer(nIDEvent);
}
