// MediaPlay2.h : main header file for the MEDIAPLAY2 application
//

#if !defined(AFX_MEDIAPLAY2_H__25B32377_F912_4316_BD71_33FAD5247EB9__INCLUDED_)
#define AFX_MEDIAPLAY2_H__25B32377_F912_4316_BD71_33FAD5247EB9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMediaPlay2App:
// See MediaPlay2.cpp for the implementation of this class
//

class CMediaPlay2App : public CWinApp
{
public:
	CMediaPlay2App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMediaPlay2App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CMediaPlay2App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

// Downloads By http://www.veryhuo.com
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MEDIAPLAY2_H__25B32377_F912_4316_BD71_33FAD5247EB9__INCLUDED_)
