#if !defined(AFX_MUSICPOSSLIDER_H__558E1612_5A65_4145_A23D_6FA81867E300__INCLUDED_)
#define AFX_MUSICPOSSLIDER_H__558E1612_5A65_4145_A23D_6FA81867E300__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MusicPosSlider.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMusicPosSlider window

class CMusicPosSlider : public CBitmapButton
{
// Construction
public:
	CMusicPosSlider();

// Attributes
public:
   void SetMusicPos(int Size);
// Operations
public:
  
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMusicPosSlider)
	//}}AFX_VIRTUAL

// Implementation
public:
	CPoint m_point;
	BOOL m_Flag;
	virtual ~CMusicPosSlider();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMusicPosSlider)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MUSICPOSSLIDER_H__558E1612_5A65_4145_A23D_6FA81867E300__INCLUDED_)
