# Microsoft Developer Studio Project File - Name="MediaPlay2" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=MediaPlay2 - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "MediaPlay2.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "MediaPlay2.mak" CFG="MediaPlay2 - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "MediaPlay2 - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe
# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /Zm660 /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x804 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x804 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386
# Begin Target

# Name "MediaPlay2 - Win32 Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\MediaPlay2.cpp
# End Source File
# Begin Source File

SOURCE=.\MediaPlay2.rc
# End Source File
# Begin Source File

SOURCE=.\MediaPlay2Dlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MusicListView.cpp
# End Source File
# Begin Source File

SOURCE=.\MusicPosSlider.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\MediaPlay2.h
# End Source File
# Begin Source File

SOURCE=.\MediaPlay2Dlg.h
# End Source File
# Begin Source File

SOURCE=.\MusicListView.h
# End Source File
# Begin Source File

SOURCE=.\MusicPosSlider.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\LastMusic.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ListView.bmp
# End Source File
# Begin Source File

SOURCE=".\res\ListVIew按下.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\Loading_6.ico
# End Source File
# Begin Source File

SOURCE=.\res\MediaPlay2.ico
# End Source File
# Begin Source File

SOURCE=.\res\MediaPlay2.rc2
# End Source File
# Begin Source File

SOURCE=.\res\NextMusic.bmp
# End Source File
# Begin Source File

SOURCE=.\res\STOP.bmp
# End Source File
# Begin Source File

SOURCE=".\res\播放(修改后).bmp"
# End Source File
# Begin Source File

SOURCE=".\res\播放按下.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\播放列表.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\播放器(修改后).bmp"
# End Source File
# Begin Source File

SOURCE=".\res\播放器.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\关闭(修改后).bmp"
# End Source File
# Begin Source File

SOURCE=".\res\关闭按下.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\删除.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\删除按下.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\上一首按下.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\声音.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\声音按钮.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\停止按下.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\位置按钮.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\位置按钮底图.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\下一首按下.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\增加目录.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\增加目录按下.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\增加音乐文件.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\增加音乐文件按下.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\最小化(修改后).bmp"
# End Source File
# Begin Source File

SOURCE=".\res\最小化按下.bmp"
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
